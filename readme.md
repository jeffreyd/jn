# jn

Jeff's notifier.  This sends SMS messages to a configurable number from a configurable number through Twilio.
It requires the Python [twilio](https://pypi.python.org/pypi/twilio "twilio on PyPi") library & setuptools.

Used for system admin notifications.

## Installation:

    cd jn && sudo python setup.py install

## Configuration:

jn.cfg.example should be renamed .jn.cfg and put in your home directory. It requires all of the following:

* sender
* default_recipient
* account (Your Twilio account SID)
* token (Your Twilio account token)

You also might want to

    chmod 600 ~/.jn.cfg

## Usage:

    jn "This is a text message"
    jn --to +15555551212 "This is a text message to a non-default recipient"
