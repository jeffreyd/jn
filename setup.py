#!env python
# This file is part of Jeff's Notifier.
#
# Jeff's Notifier is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Jeff's Notifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Jeff's Notifier.  If not, see <http://www.gnu.org/licenses/>.
import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(name='jn',
    version='1.0',
    description='Jeff Notify',
    author='Jeffrey D Johnson',
    author_email='jeffreyd@jeffreyd.org',
    license='GPLv3',
    keywords='sms notify',
    url='http://bitbucket.org/jeffreyd/jn',
    long_description=read('readme.md'),
    packages=['jnlib',],
    scripts=['scripts/jn',],
    install_requires=['twilio',]
)
