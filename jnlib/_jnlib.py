#!env python
# This file is part of Jeff's Notifier.
#
# Jeff's Notifier is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Jeff's Notifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Jeff's Notifier.  If not, see <http://www.gnu.org/licenses/>.
from twilio.rest import TwilioRestClient
from twilio.rest.exceptions import TwilioRestException

class SendException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg

class MessageSender(object):
    def __init__(self, account, token, sender):
        self.account = account
        self.token = token
        self.sender = sender

        self.client = TwilioRestClient(account=account, token=token)

    def send(self, to, msg):
        try:
            message = self.client.messages.create(to=to, from_=self.sender,
                    body=msg)
            return message
        except TwilioRestException as e:
            raise SendException(str(e))
